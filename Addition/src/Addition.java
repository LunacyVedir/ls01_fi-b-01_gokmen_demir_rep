import java.util.Scanner;
public class Addition {

	//1.Programmhinweis
	public static void programmhinweis() {
		
		System.out.println("hinweis: ");
		System.out.println("Das Programm addiert 2 eingegebene Zahlen: ");
		
	}
	
	
	//4.eingabe
	
	public static double Eingabe(String text) {
		
		Scanner sc= new Scanner(System.in);
		System.out.println(text);
		double zahl = sc.nextDouble();
		return zahl;
		

	}
	
	
	
	//3.Verarbeitung
	public static double Verarbeitung(double zahl1, double zahl2){
		double erg;
		
		 erg = zahl1 + zahl2;
		 
		 return erg;
		 
	}
	
	
	//2.Ausgabe
	public static void Ausgabe(double erg, double zahl1, double zahl2) {
	
	System.out.println("Ergebnis der Addition");
	System.out.printf("%.2f = %.2f + %.2f", erg, zahl1 , zahl2);
	
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void main(String[] args) {
		

		double zahl1 = 0.0, zahl2, erg = 0.0 ;
		
		
		programmhinweis();
	    zahl1 = Eingabe("1.Zahl");
	    zahl2 = Eingabe("2.Zahl");
	    
	    erg = Verarbeitung(zahl1 , zahl2);
	    Ausgabe (erg , zahl1 , zahl2);
		
		
	}

}
